import { PipeTransform, BadRequestException } from '@nestjs/common';
import { TaskStatus } from '../enums/task-status.enum';

export class TaskStatusValidationPipe implements PipeTransform {
  readonly allowedStatus = [
    TaskStatus.DONE,
    TaskStatus.IN_PROGRESS,
    TaskStatus.OPEN,
  ];

  private isStatusValid(status: any): boolean {
    return this.allowedStatus.includes(status);
  }

  transform(value: any) {
    const status = value.toUpperCase();

    if (!this.isStatusValid(status)) {
      throw new BadRequestException(`${value} is an invalid status`);
    }

    return status;
  }
}
