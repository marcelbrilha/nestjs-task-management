import { TaskStatus } from '../enums/task-status.enum';

export class UpdateTaskStatusDto {
  id: number;
  status: TaskStatus;
}
